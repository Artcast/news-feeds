import React from 'react';
import classes from './header.module.scss';
class Header extends React.Component{

    render(){
        return (
            <header>
                <div className={classes.header}>
                    Header
                </div>
            </header>
        );
    }
    
};

export default Header;