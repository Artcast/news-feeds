import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import Card from './card';
configure({adapter: new Adapter()});
it('renders without crashing with Creator', () => {
  shallow(<Card creator="Someone" />);
});

it('renders without crashing without Creator', () => {
  shallow(<Card />);
});