import React from 'react';
import classes from './card.module.scss';

const Card = (props) => {
    return (
        <div className={classes.card}>
            <div className={classes.title}>{props.title}</div>
            <div className={classes.date}>{props.pubDate}</div>
            <div className={classes.desc}>{props.content}</div>
            <div className={classes.creator}><span>Created by:</span>{props.creator?props.creator:'Anonymous'}</div>
        </div>
    );
};

export default Card;
