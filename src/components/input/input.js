import React from 'react';
import classes from './input.module.scss';

const Input = (props) => {
    return (
        <div className={classes.input}>
            <input onChange={props.onChange} value={props.value} type="text" placeholder={props.placeholder}/>
        </div>
    );
};

export default Input;
