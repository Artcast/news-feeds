import React from 'react';
import classes from './footer.module.scss';

const Footer = (props) => {
    return (
        <footer>
            <div className={classes.footer}>
                Footer
            </div>
        </footer>
    );
};

export default Footer;
