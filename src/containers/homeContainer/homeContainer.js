import React from 'react';
import CreateFeedContainer from './createFeedContainer/createFeedContainer';
import FeedsContainer from './feedsContainer/feedsContainer';

const HomeContainer = () => {
    return (
        <div>
            <CreateFeedContainer/>
            <FeedsContainer/>
        </div>
    );
};

export default HomeContainer;
