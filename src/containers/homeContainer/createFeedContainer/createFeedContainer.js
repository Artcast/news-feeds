import React from 'react';
import Input from '../../../components/input/input';
import Button from '../../../components/button/button';
import classes from './createFeedContainer.module.scss';
import { connect } from 'react-redux';
import { fetchFeeds } from '../../../reducers/feedReducer';

class CreateFeedContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            url: '',
            lFeed: []
        }
    }

    componentDidMount() {
        let lFeed = JSON.parse(localStorage.getItem('lFeed'));
        if (lFeed === null) {
            lFeed = []
        }
        this.setState({ lFeed: lFeed });
    }

    handleAddFeed = () => {
        const { name, url, lFeed } = this.state;
        if (name && url) {
            lFeed.push({ name: name, url: url });
            this.setState({ name:'',url:'',lFeed: lFeed });
            localStorage.setItem('lFeed', JSON.stringify(lFeed));
        }
        else{
            alert('Must fill name and URL');
        }
    }

    updateFeedValues = value => e => {
        this.setState({
            [value]: e.target.value
        });
    }

    renderRSS = url => e => {
        this.props.fetchFeeds(url);
    }

    render() {
        const { name, url, lFeed } = this.state;
        const lFeedDiv = lFeed.map((feed, index) => <div onClick={this.renderRSS(feed.url)} className={classes.feed} key={index}>{feed.name}</div>);
        return (
            <div className={classes.createFeedContainer}>
                <h2>Create Feed</h2>
                <Input value={name} onChange={this.updateFeedValues('name')} placeholder="Feed Name" />
                <Input value={url} onChange={this.updateFeedValues('url')} placeholder="Feed URL" />
                <div className={classes.button}>
                    <Button onClick={this.handleAddFeed} label="Add Feed" />
                </div>
                <h3>My Feeds</h3>
                <div className={classes.feedContainer}>
                    {lFeedDiv}
                </div>
            </div>
        );
    }

};


const mapStateToProps = (state) => ({
    feeds: state.feeds
});

const actionMakers = {
    fetchFeeds
}


export default connect(mapStateToProps, actionMakers)(CreateFeedContainer);
