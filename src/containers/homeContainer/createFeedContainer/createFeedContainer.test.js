import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { configure } from 'enzyme';
import CreateFeedContainer from './createFeedContainer';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import reducers from '../../../reducers';
import { createStore, applyMiddleware } from 'redux';
import ReactDOM from 'react-dom';

const store = createStore(reducers, applyMiddleware(thunk));
configure({ adapter: new Adapter() });
describe('CreateFeedContainer test cases...', () => {
	it('should render without crashing', () => {
		const mockContainer = document.createElement('div');

		ReactDOM.render(
			<Provider store={store}>
				<CreateFeedContainer />
			</Provider>,
			mockContainer
		);
		ReactDOM.unmountComponentAtNode(mockContainer);
	});
});
