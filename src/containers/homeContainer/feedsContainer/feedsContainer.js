import React from 'react';
import { connect } from 'react-redux';
import Card from '../../../components/card/card';
import classes from './feedsContainer.module.scss';
import  constant  from '../../../constants/constants';

class FeedsContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            from: 0,
            to: constant.PAGE_TO_SHOW
        }
    }

    componentDidMount() {
        this.refs.iScroll.addEventListener("scroll", () => {
            if (Math.ceil(this.refs.iScroll.scrollTop + this.refs.iScroll.clientHeight) >= this.refs.iScroll.scrollHeight) {
                const { to } = this.state;
                this.setState({ to: to + constant.PAGE_TO_SHOW });
            }
        });
    }

    render() {
        const { from, to } = this.state;
        const lCards = 'items' in this.props.feeds.feeds ? 
        this.props.feeds.feeds.items.slice(from, to).map((feed, index) => <Card key={index} {...feed} />) :
        null;
        return (
            <div>
                <h3>List Feeds</h3>
                <div ref="iScroll" className={classes.iScroll}>
                    {lCards}
                </div>
            </div>
        );
    }

};


const mapStateToProps = (state) => ({
    feeds: state.feeds
});

export default connect(mapStateToProps, null)(FeedsContainer);
