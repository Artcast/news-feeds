import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import FeedsContainer from './feedsContainer';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import reducers from '../../../reducers';
import { createStore, applyMiddleware } from 'redux';
import ReactDOM from 'react-dom';
const store = createStore(reducers, applyMiddleware(thunk));
configure({ adapter: new Adapter() });
describe('CreateFeedContainer test cases...', () => {
    const feeds = {
        feeds:{
            items:
            [
                {
                    "title":"These step-back 3s are sent from another planet",
                    "link":"http://www.espn.com/nba/story/_/id/26118445/these-step-back-3s-sent-another-planet",
                    "pubDate":"Sun, 3 Mar 2019 07:32:56 EST",
                    "content":"How is James Harden doing this? Get ready, there's more coming.",
                    "contentSnippet":"How is James Harden doing this? Get ready, there's more coming.",
                    "guid":"26118445",
                    "isoDate":"2019-03-03T12:32:56.000Z"
                }
            ]
        }
    }
    it('should render without crashing', () => {
		const mockContainer = document.createElement('div');

		ReactDOM.render(
			<Provider store={store}>
				<FeedsContainer />
			</Provider>,
			mockContainer
		);
		ReactDOM.unmountComponentAtNode(mockContainer);
	});
    it('renders with list of feeds', () => {        
        let wrapper;
        beforeEach(() => {
            wrapper = shallow(<FeedsContainer feeds={feeds} />);
        });
    });  
});