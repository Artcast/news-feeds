import { combineReducers } from 'redux';
import Feeds from './feedReducer';


const rootReducer = combineReducers({
  feeds: Feeds,
});

export default rootReducer;