const RECEIVE_FEEDS = 'RECEIVE_FEEDS';
const REQUEST_FEEDS = 'REQUEST_FEEDS';

export function fetchFeeds(url) {
  return (dispatch) => {
    const lFeed = JSON.parse(localStorage.getItem(url));
    if(lFeed!==null){
      dispatch(receiveFeeds(lFeed))
      return;
    }
    dispatch(requestFeeds());
    let Parser = require('rss-parser');
    let parser = new Parser();
    (async () => {
      const lFeed = await parser.parseURL(url);
      localStorage.setItem(url, JSON.stringify(lFeed));
      dispatch(receiveFeeds(lFeed))
    })();
  }
}

export function receiveFeeds(feeds) {
  return {
    type: RECEIVE_FEEDS,
    feeds
  }
}

export function requestFeeds() {
  return {
    type: REQUEST_FEEDS
  }
}

const initialState = {
  fetching: false,
  feeds: []
}

export default function (state = initialState, action) {
  switch (action.type) {
    case RECEIVE_FEEDS:
      return {
        fetching: true,
        feeds: action.feeds
      }
    default:
      return state;
  }
}